﻿using Meritokrat2.Models;
using Meritokrat2.Models.AppModels;
using Meritokrat2.Models.AppModels.Kviz;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Meritokrat2.Data_Layer
{
    public class Repo : IRepo
    {
        internal Repo()
        {

        }


        #region Lekcija
        public Lekcija GetLekcija(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Lekcijas.Where(x => x.LekcijaId == id).FirstOrDefault();
            }
        }

        public IList<Lekcija> GetLekcije(int razred)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Lekcijas
                    .Where(x => x.RBLekcije > 0 && x.Razred == razred)
                    .OrderBy(x => x.RBLekcije)
                    .ToList();
            }
        }

        public void AddLekcija(Lekcija lekcija)
        {
            using (var db = new ApplicationDbContext())
            {
                lekcija.RBLekcije = GetNoviRBZaLekciju(lekcija.Razred);
                db.Lekcijas.Add(lekcija);
                db.SaveChanges();
            }
        }

        public string GetProsjekLekcije(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var maxBodovi = GetLekcija(id).MaxBodovi;
                var uceniciLekcije = db.UcenikLekcijas.Where(x => x.IdLekcija == id);
                if (uceniciLekcije.Count() == 0)
                {
                    return "Još nema";
                }
                var average = (double)uceniciLekcije.Average(x => x.Bodovi);
                return $"{average.ToString("#.##")} / {maxBodovi}";
            }
        }

        private int GetNoviRBZaLekciju(int razred)
        {
            using (var db = new ApplicationDbContext())
            {
                var lekcije = db.Lekcijas.Where(x => x.Razred == razred);
                if (lekcije.Count() > 0)
                {
                    return lekcije.Max(x => x.RBLekcije) + 1;
                }
                return 1;
            }
        }

        private Lekcija GetLekcijaPoRB(int rb, int razred)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Lekcijas.Where(x => x.RBLekcije == rb && x.Razred == razred).FirstOrDefault();
            }
        }

        public void UpdateLekcija(Lekcija lekcija)
        {
            using (var db = new ApplicationDbContext())
            {
                var _lekcija = db.Lekcijas.Find(lekcija.LekcijaId);
                _lekcija.Naziv = lekcija.Naziv;
                _lekcija.IdVideo = lekcija.IdVideo;
                db.SaveChanges();
            }
        }

        public void IncreaseLekcijaOdigrana(int lekcijaId)
        {
            using (var db = new ApplicationDbContext())
            {
                var _lekcija = db.Lekcijas.Find(lekcijaId);
                _lekcija.RijesenaPuta++;
                db.SaveChanges();
            }
        }
        #endregion

        #region AspKorisnik
        public String DeleteUser(String id)
        {
            using (var db = new ApplicationDbContext())
            {
                var mod = db.Users.Find(id);
                var email = mod.Email;
                if (mod != null)
                {
                    db.Users.Remove(mod);
                    db.SaveChanges();
                }
                return email;
            }
        }
        #endregion


        #region Ucenik
        public Ucenik GetUcenik(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Uceniks
                    .Where(x => x.UcenikId == id)
                    .FirstOrDefault();
            }
        }

        public int GetRezultatUcenika(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var lekcije = db.UcenikLekcijas
                    .Where(x => x.IdUcenik == id);
                if (lekcije.Count() > 0)
                {
                    return lekcije.Sum(x => x.Bodovi);
                }
                return 0;
            }
        }

        public Lekcija GetSljedecaLekcijaZaUcenika(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var ucenik = db.Uceniks.Find(id);
                var query = from ul in db.UcenikLekcijas
                            join l in db.Lekcijas on ul.IdLekcija equals l.LekcijaId
                            where ul.IdUcenik == id
                            orderby l.RBLekcije descending
                            select l;
                if (query.Count() > 0)
                {
                    return GetLekcijaPoRB(query.First().RBLekcije + 1, ucenik.Razred);
                }
                else
                {
                    return GetLekcijaPoRB(1, ucenik.Razred);
                }
            }
        }

        public void SetZadnjeOdigrano(int ucenikId)
        {
            using (var db = new ApplicationDbContext())
            {
                var ucenik = db.Uceniks.Find(ucenikId);
                if (ucenik != null)
                {
                    ucenik.ZadnjaOdigranaIgra = DateTime.Now;
                    db.SaveChanges();
                }
            }
        }
        #endregion


        #region UcenikLekcija
        public IList<UcenikLekcija> GetRijeseneLekcije(int ucenikId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.UcenikLekcijas.Where(x => x.IdUcenik == ucenikId).ToList();
            }

        }

        public void AddUcenikLekcija(UcenikLekcija ucenikLekcija)
        {
            using (var db = new ApplicationDbContext())
            {
                db.UcenikLekcijas.Add(ucenikLekcija);
                db.SaveChanges();
            }
        }

        public bool MozeLiUcenikVidjetiLekciju(int lekcijaId, int ucenikId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.UcenikLekcijas.Where(x => x.IdLekcija == lekcijaId && x.IdUcenik == ucenikId).Count() > 0 ? true : false;
            }
        }

        public UcenikLekcija GetUcenikLekcija(int lekcijaId, int ucenikId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.UcenikLekcijas.Where(x => x.IdLekcija == lekcijaId && x.IdUcenik == ucenikId).First();
            }
        }

        public String GetBiljeskeZaUcenikaLekciju(int ucenikId, int lekcijaId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.UcenikLekcijas.Where(x => x.IdLekcija == lekcijaId && x.IdUcenik == ucenikId).First().Biljeske;
            }
        }

        #endregion


        #region Rezultati
        public List<RezultatiPojedinacno> GetRezultatiPojedinacno()
        {
            using (var db = new ApplicationDbContext())
            {
                var result = db.Database
                    .SqlQuery<RezultatiPojedinacno>("GetRezultatiPojedinacno")
                    .ToList();
                return result;
            }

        }

        public List<RezultatiSkole> GetRezultatiPoSkolama()
        {
            using (var db = new ApplicationDbContext())
            {
                var result = db.Database
                    .SqlQuery<RezultatiSkole>("GetRezultatiPoSkolama")
                    .ToList();
                return result;
            }
        }
        #endregion


        #region Kviz
        public Kviz GetKviz(int id)
        {
            if (id == 0)
            {
                return null;
            }
            using (var db = new ApplicationDbContext())
            {
                return db.Kvizs.Find(id);
            }
        }

        public void AddKviz(Kviz kviz)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Kvizs.Add(kviz);
                db.SaveChanges();
            }
        }

        public void SetKvizForLekcija(int lekcijaId, int kvizId)
        {
            using (var db = new ApplicationDbContext())
            {
                var lekcija = db.Lekcijas.Find(lekcijaId);
                if (lekcija != null)
                {
                    lekcija.IdKviz = kvizId;
                    db.SaveChanges();
                }
            }
        }

        public Kviz GetKvizForLekcija(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return GetKviz(db.Lekcijas.Where(x => x.LekcijaId == id).FirstOrDefault().IdKviz);
            }
        }
        #endregion


        #region Pitanje
        public void AddPitanje(Pitanje pitanje)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Pitanjes.Add(pitanje);
                db.SaveChanges();
            }
        }

        public List<Pitanje> GetPitanjaZaKviz(int kvizId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Pitanjes.Where(x => x.IdKviz == kvizId).ToList();
            }
        }

        public void DeletePitanje(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                var pitanje = db.Pitanjes.Find(id);
                if (pitanje != null)
                {
                    db.Pitanjes.Remove(pitanje);
                    db.SaveChanges();
                }
            }
        }

        public List<Pitanje> Get5KvizPitanja(int idKviz)
        {
            using (var db = new ApplicationDbContext())
            {
                var pitanja = db.Pitanjes.Where(x => x.IdKviz == idKviz).OrderBy(q => Guid.NewGuid()); //random order
                if (pitanja.Count() > 0)
                {
                    return pitanja.Take(5).ToList();
                }
                return pitanja.ToList();
            }
        }

        public Pitanje GetPitanje(int pitanjeId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Pitanjes.Find(pitanjeId);
            }
        }

        public List<Odgovor> GetOdgovoriZaPitanje(int pitanjeId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Odgovors.Where(x => x.IdPitanje == pitanjeId).OrderBy(q => Guid.NewGuid()).ToList();
            }
        }

        public void AddOdgovor(Odgovor odgovor)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Odgovors.Add(odgovor);
                db.SaveChanges();
            }
        }
        #endregion


        #region Clanak
        public void AddClanak(Clanak clanak)
        {
            using (var db = new ApplicationDbContext())
            {

                db.Clanaks.Add(clanak);
                db.SaveChanges();
            }
        }

        public void AddPost(Post post)
        {
            using (var db =new ApplicationDbContext())
            {
                db.Posts.Add(post);
                db.SaveChanges();
            }
        }

        List<Clanak> IRepo.GetClanke()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Clanaks.Where(x => x.ClanakFileName != "").ToList();
            }
        }

        public int DeleteClanak(int idClanak)
        {
            using (var db=new ApplicationDbContext())
            {
                var Clanak = db.Clanaks.Single(c=>c.ClanakID==idClanak);
                var idReturn = db.Clanaks.Single(c => c.ClanakID == idClanak).ClanakID;

                if (Clanak!=null)
                {
                    db.Clanaks.Remove(Clanak);
                    db.SaveChanges();
                }
                return idClanak;

            }
        
        }
        #endregion


        #region Moderatori
        public ApplicationUser GetModerator(string id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Users
                    .Include(x => x.Roles)
                    .Where(x=>x.Id == id)
                    .FirstOrDefault();
            }
        }

        public IList<ApplicationUser> GetModerators()
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Users
                    .Include(x => x.Roles)
                    .ToList();
            }
        }
        #endregion


        #region Aktivnost
        public void AddAktivnost(Aktivnost aktivnost)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Aktivnosts.Add(aktivnost);
                db.SaveChanges();
            }
        }

        public IList<Aktivnost> GetAktivnostZaKorisnika(String userId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Aktivnosts
                    .Where(x => x.UserId == userId)
                    .ToList();
            }
        }

   
        #endregion
    }


}