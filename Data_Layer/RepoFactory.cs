﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meritokrat2.Data_Layer
{
    public static class RepoFactory
    {
        private static IRepo repo;

        public static IRepo GetRepo()
        {
            if (repo == null)
            {
                repo = new Repo();
            }
            return repo;
        }
    }
}