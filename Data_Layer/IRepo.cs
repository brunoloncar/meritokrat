﻿using Meritokrat2.Models;
using Meritokrat2.Models.AppModels;
using Meritokrat2.Models.AppModels.Kviz;
using System;
using System.Collections.Generic;

namespace Meritokrat2.Data_Layer
{
    public interface IRepo
    {
        #region Lekcija
        IList<Lekcija> GetLekcije(int razred);
        string GetProsjekLekcije(int id);
        Lekcija GetLekcija(int id);
        void AddLekcija(Lekcija lekcija);
        #endregion

        #region AspKorisnik
        String DeleteUser(String id);
        #endregion

        #region Ucenik
        Ucenik GetUcenik(int id);
        int GetRezultatUcenika(int id);
        Lekcija GetSljedecaLekcijaZaUcenika(int ucenikId);
        void SetZadnjeOdigrano(int ucenikId);
        #endregion

        #region UcenikLekcija
        IList<UcenikLekcija> GetRijeseneLekcije(int ucenikId);
        void AddUcenikLekcija(UcenikLekcija ucenikLekcija);
        bool MozeLiUcenikVidjetiLekciju(int lekcijaId, int ucenikId);
        UcenikLekcija GetUcenikLekcija(int lekcijaId, int ucenikId);
        String GetBiljeskeZaUcenikaLekciju(int ucenikId, int lekcijaId);

        #endregion

        #region Rezultati
        List<RezultatiPojedinacno> GetRezultatiPojedinacno();
        List<RezultatiSkole> GetRezultatiPoSkolama();
        void UpdateLekcija(Lekcija lekcija);

        #endregion

        #region Kviz
        Kviz GetKviz(int id);
        void AddKviz(Kviz kviz);
        void SetKvizForLekcija(int lekcijaId, int kvizId);
        Kviz GetKvizForLekcija(int id);
        void AddPitanje(Pitanje pitanje);
        void AddOdgovor(Odgovor odgovor);
        List<Pitanje> GetPitanjaZaKviz(int kvizId);
        List<Odgovor> GetOdgovoriZaPitanje(int pitanjeId);
        void DeletePitanje(int id);
        List<Pitanje> Get5KvizPitanja(int idKviz);
        Pitanje GetPitanje(int pitanjeId);
        void IncreaseLekcijaOdigrana(int lekcijaId);
        #endregion


        #region Clanak
        void AddClanak(Clanak clanak);
        List<Clanak> GetClanke();
        int DeleteClanak(int id);

        #endregion

        #region Moderatori
        ApplicationUser GetModerator(string id);
        IList<ApplicationUser> GetModerators();
        #endregion

        #region Aktivnost
        void AddAktivnost(Aktivnost aktivnost);

        IList<Aktivnost> GetAktivnostZaKorisnika(String userId);
        #endregion

        #region posts
        void AddPost(Post post);
        #endregion
    }
}