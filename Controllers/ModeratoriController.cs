﻿using Meritokrat2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Meritokrat2.Models.AppModels;

namespace Meritokrat2.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ModeratoriController : MyController
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Pregled(string id)
        {
            ViewBag.Aktivnosti = Repository.GetAktivnostZaKorisnika(id);
            return View(Repository.GetModerator(id));
        }

        [HttpGet]
        public ActionResult Dodaj()
        {
            return View();
        }

        //modifikacija account/register metode
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Dodaj(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var currentUser = UserManager.FindByName(user.UserName);
                    var roleresult = UserManager.AddToRole(currentUser.Id, "Moderator");
                    Repository.AddAktivnost(new Aktivnost(user.Id, "Korisnik dodan u sustav.", true));
                    Repository.AddAktivnost(new Aktivnost(User.Identity.GetUserId(), $"Dodaje moderatora: {user.Email}.", true));
                    return RedirectToAction("Index", "Sustav", new { msg = "add" });
                }
                AddErrors(result);
            }
            return View(model);
        }

        public ActionResult Izbrisi(String id)
        {
            var email = Repository.DeleteUser(id);
            Repository.AddAktivnost(new Aktivnost(User.Identity.GetUserId(), $"Briše moderatora: {email}.", true));
            return RedirectToAction("Index", "Sustav", new { msg = "delete" });
        }
    }
}