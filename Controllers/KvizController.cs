﻿using Meritokrat2.Models.AppModels;
using Meritokrat2.Models.AppModels.Kviz;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Meritokrat2.Controllers
{
    public class KvizController : MyController
    {
        [Authorize(Roles = "Administrator,Moderator")]
        public ActionResult Pregled(int id)
        {
            //id je id lekcije za koju se dohvaca kviz
            var lekcija = Repository.GetLekcija(id);
            ViewBag.Lekcija = lekcija;

            var kviz = Repository.GetKviz(lekcija.IdKviz);
            ViewBag.Kviz = kviz;
            List<Pitanje> pitanja = null;
            if (kviz != null)
            {
                pitanja = Repository.GetPitanjaZaKviz(kviz.KvizId);
            }
            return View(pitanja);
        }

        [HttpGet]
        public ActionResult Dodaj(int id)
        {
            var lekcija = Repository.GetLekcija(id);
            ViewBag.Lekcija = lekcija;
            return View();
        }

        [HttpPost]
        public ActionResult Dodaj(int LekcijaId, int dummy = 0)
        {
            var keys = Request.Form.AllKeys;

            List<Pitanje> pitanja = new List<Pitanje>();
            List<int> doneNumbers = new List<int>();

            var kviz = Repository.GetKvizForLekcija(LekcijaId);
            
            foreach (var key in keys)
            {
                if (key.Contains("_"))
                {
                    string[] splitKey = key.Split('_');
                    int odgovorBroj = int.Parse(splitKey[1]);
                    if (!doneNumbers.Contains(odgovorBroj))
                    {
                        String tekstPitanja = Request.Form[keys.Where(x => x == $"tekst_{odgovorBroj}").FirstOrDefault().Trim()];
                        PitanjeType tip = ((PitanjeType)Enum.Parse(typeof(PitanjeType), Request.Form[keys.Where(x => x == $"tip_{odgovorBroj}").FirstOrDefault()]));

                        Pitanje pitanje = new Pitanje()
                        {
                            TekstPitanja = tekstPitanja,
                            Tip = tip,
                            IdKviz = kviz.KvizId,
                            Vrijednost = 1 //dodati podrsku za vrijednost
                        };
                        Repository.AddPitanje(pitanje);

                        String _odgovori = Request.Form[keys.Where(x => x == $"odgovor_{odgovorBroj}").FirstOrDefault()];
                        //ako je više odgovora
                        if (_odgovori.Contains("\r\n"))
                        {
                            string[] odgovori = _odgovori.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var _odgovor in odgovori)
                            {
                                DodajOdgovor(pitanje, _odgovor);
                            }
                        }
                        //ako je samo jedan odgovor
                        else
                        {
                            DodajOdgovor(pitanje, _odgovori);
                        }
                        doneNumbers.Add(odgovorBroj);
                    }
                }
            }
            Repository.AddAktivnost(new Aktivnost(User.Identity.GetUserId(), $"Dodaje pitanja za lekciju (id: {LekcijaId}).", true));
            return RedirectToAction("Pregled", new { id = LekcijaId });
        }

        private void DodajOdgovor(Pitanje pitanje, string _odgovor)
        {
            Odgovor odgovor = new Odgovor();
            odgovor.IdPitanje = pitanje.PitanjeId;
            if (_odgovor.Trim().Length > 0)
            {
                if (_odgovor[0] == '*')
                {
                    odgovor.Tocan = true;
                    odgovor.TekstOdgovora = _odgovor.Substring(1);
                }
                else
                {
                    odgovor.Tocan = false;
                    odgovor.TekstOdgovora = _odgovor;
                }
                if (pitanje.Tip == PitanjeType.Nadopuni)
                {
                    odgovor.Tocan = true;
                }
                Repository.AddOdgovor(odgovor);
            }
        }


    }
}