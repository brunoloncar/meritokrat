﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Meritokrat2.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class SustavController : MyController
    {
        public ActionResult Index(string msg = null)
        {
            ViewBag.Moderatori = Repository.GetModerators();
            TempData["ToastrMsg"] = msg ?? "null";
            return View();
        }
    }
}