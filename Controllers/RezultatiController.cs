﻿using Meritokrat2.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Meritokrat2.Controllers
{
    [UcenikAuthorize]
    public class RezultatiController : MyController
    {
        public ActionResult Index()
        {
            ViewBag.Pojedinacni = Repository.GetRezultatiPojedinacno();
            ViewBag.PoSkolama = Repository.GetRezultatiPoSkolama();
            return View();
        }
    }
}