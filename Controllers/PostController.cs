﻿using Meritokrat2.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Meritokrat2.Controllers
{
    public class PostController : MyController
    {
        // GET: Post
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddPost() => View();

        [HttpPost,ValidateInput(false)]
        public ActionResult AddPost(Post post) {
            Repository.AddPost(post);
            return RedirectToAction("AddPost","Post");
        }
    }
}