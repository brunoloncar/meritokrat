﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Meritokrat2.Models.AppModels;
using Meritokrat2.Data_Layer;
using Microsoft.AspNet.Identity;
using Microsoft.WindowsAPICodePack.Shell;
using System.Drawing;

namespace Meritokrat2.Controllers
{
    [Authorize(Roles = "Administrator,Moderator")]
    public class ClanakController : MyController
    {
        // GET: Clanak
        //PREGLED ZA MODERATORE
        [Authorize(Roles = "Administrator,Moderator")]
        public ActionResult Index()
        {
            return View(Repository.GetClanke());
        }

        public ActionResult PregledClanaka()
        {

            return View(Repository.GetClanke());
        }


        public ActionResult DodajClanak() {

            return View();
        }

        [Authorize(Roles = "Administrator,Moderator")]
        [HttpPost]
        public ActionResult DodajClanak(String Autor,string Naslov) {

            
            var clanak = new Clanak {
                Naslov=Naslov,
                Autor=Autor
            };

            String clanakFileName = HandleUploadedClanak(Request);
            if (clanakFileName!=null)
            {
                clanak.ClanakFileName = clanakFileName;
            }

            Repository.AddClanak(clanak);

            TempData["message"] = "Added";
            Repository.AddAktivnost(new Aktivnost(User.Identity.GetUserId(), $"Dodaje članak: {clanak.Naslov}.", true));
            return RedirectToAction("index","Clanak");
        }
        [Authorize(Roles = "Administrator,Moderator")]
        [HttpGet]
        public ActionResult Download(int id) {

            var clanak = Repository.GetClanke().Single(s=>s.ClanakID==id);
            String fileName = clanak.ClanakFileName;
            if (fileName!=null||fileName!="")
            {
                byte[] clanakFile = System.IO.File.ReadAllBytes(fileName);
                string name = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                return File(clanakFile, System.Net.Mime.MediaTypeNames.Application.Octet, name);
            }
            //TOASTER TEMPDATA KASNIJE
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrator,Moderator")]
        public ActionResult DeleteClanak(int idClanak) {


            if (Repository.DeleteClanak(idClanak) != -1)
            {
                TempData["message"] = "Deleted";
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
            
        }

        private string HandleUploadedClanak(HttpRequestBase request)
        {
            if (request.Files[0]!=null)
            {
                var httpPostedFile = request.Files[0];
                if (httpPostedFile!=null&&httpPostedFile.FileName.ToString()!="")
                {
                    // Get the complete file path
                    var uploadFilesDir = System.Web.HttpContext.Current.Server.MapPath("~/Content/Clanci");
                    if (!Directory.Exists(uploadFilesDir))
                    {
                        Directory.CreateDirectory(uploadFilesDir);
                    }
                    var fileSavePath = Path.Combine(uploadFilesDir, httpPostedFile.FileName);

                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath);
                    return fileSavePath;
                }
            }
            return null;
        }
    }
}