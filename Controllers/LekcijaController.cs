﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Meritokrat2.Models;
using Meritokrat2.Models.AppModels;
using Meritokrat2.Models.AppModels.Kviz;
using Microsoft.AspNet.Identity;

namespace Meritokrat2.Controllers
{
    [Authorize(Roles = "Administrator,Moderator")]
    public class LekcijaController : MyController
    {
        public ActionResult Index(int id)
        {
            return View(Repository.GetLekcije(id));
        }

        public ActionResult Pregled(int id)
        {
            Lekcija lekcija = Repository.GetLekcija(id);
            return View(lekcija);
        }

        #region Uređivanje lekcije
        [HttpGet]
        public ActionResult Uredi(int id)
        {
            var lekcija = Repository.GetLekcija(id);
            return View(lekcija);
        }

        [HttpPost]
        public ActionResult Uredi(Lekcija lekcija)
        {

            var _lekcija = Repository.GetLekcija(lekcija.LekcijaId);
            _lekcija.Naziv = lekcija.Naziv;

            if (_lekcija.IdVideo != Guid.Empty)
            {
                HandleUploadedVideo(Request, _lekcija.IdVideo.ToString());
            }
            else
            {
                _lekcija.IdVideo = (Guid)HandleUploadedVideo(Request);
            }
            Repository.UpdateLekcija(_lekcija);
            Repository.AddAktivnost(new Aktivnost(User.Identity.GetUserId(), $"Uređuje lekciju (id: {lekcija.LekcijaId}).", true));
            return RedirectToAction("Pregled", new { id = lekcija.LekcijaId });
        }
        #endregion

        #region Dodavanje Lekcije
        [HttpGet]
        public ActionResult Dodaj()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Dodaj(String Naziv, int Razred)
        {
            var lekcija = new Lekcija
            {
                Naziv = Naziv,
                Razred = Razred,
                MaxBodovi = 5
            };


            //primanje videa u uploadu
            Guid? videoGuid = HandleUploadedVideo(Request);
            if (videoGuid != null)
            {
                lekcija.IdVideo = (Guid)videoGuid;
            }
            Repository.AddLekcija(lekcija);

            //dodavanje kviza za lekciju
            Kviz kviz = new Kviz();
            Repository.AddKviz(kviz);
            Repository.SetKvizForLekcija(lekcija.LekcijaId, kviz.KvizId);
            Repository.AddAktivnost(new Aktivnost(User.Identity.GetUserId(), $"Dodaje lekciju (id: {lekcija.LekcijaId}).", true));

            return RedirectToAction("pregled", "lekcija", new { id = lekcija.LekcijaId });
        }

        private Guid? HandleUploadedVideo(HttpRequestBase request, string _guid = null)
        {
            if (request.Files[0] != null)
            {
                var httpPostedFile = request.Files[0];
                if (httpPostedFile != null && httpPostedFile.FileName.ToString() != "")
                {
                    string guid;
                    if (_guid == null)
                    {
                        guid = Guid.NewGuid().ToString();
                    } else
                    {
                        guid = _guid;
                    }

                    // Get the complete file path
                    var uploadFilesDir = System.Web.HttpContext.Current.Server.MapPath("~/Content/Videos");
                    if (!Directory.Exists(uploadFilesDir))
                    {
                        Directory.CreateDirectory(uploadFilesDir);
                    }
                    var fileSavePath = Path.Combine(uploadFilesDir, guid.ToString() + httpPostedFile.FileName.Substring(httpPostedFile.FileName.LastIndexOf('.')));

                    // Save the uploaded file to "UploadedFiles" folder
                    httpPostedFile.SaveAs(fileSavePath);
                    return Guid.Parse(guid);
                }
            }
            return null;
        }
        #endregion
    }
}


