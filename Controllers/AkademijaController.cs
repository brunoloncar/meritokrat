﻿using Meritokrat2.Models.AppModels;
using Meritokrat2.Models.AppModels.Kviz;
using Meritokrat2.Models.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Meritokrat2.Controllers
{
    [UcenikAuthorize]
    public class AkademijaController : MyController
    {
        public ActionResult Index()
        {
            var ucenik = Repository.GetUcenik(1);
            ViewBag.Ucenik = ucenik;

            var lekcije = Repository.GetRijeseneLekcije(ucenik.UcenikId).ToList();
            ViewBag.Lekcije = lekcije;
            ViewBag.SljedecaLekcija = Repository.GetSljedecaLekcijaZaUcenika(ucenik.UcenikId);
            return View();
        }

        [System.Web.Http.HttpGet]
        public ActionResult Igraj()
        {
            var ucenik = Repository.GetUcenik(1);
            if (ucenik.MozeIgrati())
            {
                var lekcija = Repository.GetSljedecaLekcijaZaUcenika(ucenik.UcenikId);
                ViewBag.Lekcija = lekcija;
                var pitanja = Repository.Get5KvizPitanja(lekcija.IdKviz);
                ViewBag.Pitanja = pitanja;
                return View();
            }
            return RedirectToAction("Index");

        }

        [System.Web.Http.HttpPost]
        public ActionResult _Igraj(int lekcijaId)
        {
            var ucenik = Repository.GetUcenik(1);
            var keys = Request.Form.AllKeys;
            var bodovi = VerifyOdgovoriAndGetBodovi(keys);
            var ucenikLekcija = new UcenikLekcija()
            {
                IdLekcija = lekcijaId,
                IdUcenik = ucenik.UcenikId,
                Bodovi = bodovi,
                Biljeske = Request.Form["biljeske"],
                DatumRjesavanja = DateTime.Now
            };
            if (!Repository.GetRijeseneLekcije(ucenik.UcenikId).Contains(ucenikLekcija) && ucenik.MozeIgrati())
            {
                Repository.AddUcenikLekcija(ucenikLekcija);
                Repository.SetZadnjeOdigrano(ucenik.UcenikId);
                Repository.IncreaseLekcijaOdigrana(lekcijaId);
            }
            return RedirectToAction("Lekcija", new { id=lekcijaId, rjesena = true });
        }

        private int VerifyOdgovoriAndGetBodovi(string[] keys)
        {
            int bodovi = 0;
            foreach (var key in keys)
            {
                if (int.TryParse(key, out int _key))
                {
                    var pitanje = Repository.GetPitanje(_key);
                    if (pitanje.Tip == PitanjeType.Jedan_tocan)
                    {
                        var odgovori = Repository
                            .GetOdgovoriZaPitanje(pitanje.PitanjeId)
                            .Where(x => x.Tocan == true).FirstOrDefault();
                        if (int.TryParse(Request.Form[key], out int odgovorId))
                        {
                            if (odgovori?.OdgovorId == odgovorId)
                            {
                                bodovi += pitanje.Vrijednost;
                            }
                        }
                    }
                    else if (pitanje.Tip == PitanjeType.Vise_tocnih)
                    {
                        var odgovori = Repository
                            .GetOdgovoriZaPitanje(pitanje.PitanjeId)
                            .Where(x => x.Tocan == true);

                        var values = Request.Form[key];
                        var valuesCount = values.Count(c => c == ',');
                        if (odgovori.Count() == valuesCount + 1)
                        {
                            if (valuesCount > 0)
                            {
                                bool flag = false;
                                foreach (var _o in odgovori)
                                {
                                    if (!values.Contains(_o.OdgovorId.ToString()))
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (flag == false)
                                {
                                    {
                                        bodovi += pitanje.Vrijednost;
                                    }
                                }
                            }
                        }
                    }
                    else if (pitanje.Tip == PitanjeType.Nadopuni)
                    {
                        var odgovori = Repository
                            .GetOdgovoriZaPitanje(pitanje.PitanjeId);

                        var tekst = Request.Form[key];
                        bool flag = false;
                        foreach (var _o in odgovori)
                        {
                            if (_o.TekstOdgovora.ToLower().Trim() == tekst.ToLower().Trim())
                            {
                                flag = true;
                                break;
                            }
                        }
                        if (flag == true)
                        {
                            bodovi += pitanje.Vrijednost;
                        }
                    }
                }

            }
            return bodovi;
        }

        public ActionResult Lekcija(int id, bool rjesena = false)
        {
            var ucenik = Repository.GetUcenik(1);
            if (Repository.MozeLiUcenikVidjetiLekciju(id, ucenik.UcenikId) == false)
            {
                var msg = new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Ne možete vidjeti ovu lekciju." };
                throw new HttpResponseException(msg);
            } else
            {
                var lekcija = Repository.GetLekcija(id);
                ViewBag.UcenikLekcija = Repository.GetUcenikLekcija(lekcija.LekcijaId, ucenik.UcenikId);
                ViewBag.Biljeske = Repository.GetBiljeskeZaUcenikaLekciju(ucenik.UcenikId, id);
                ViewBag.Rjesena = rjesena;
                return View(lekcija);
            }
        }
    }
}