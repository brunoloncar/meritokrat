﻿using Meritokrat2.Models.AppModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Meritokrat2.Controllers
{
    [Authorize(Roles = "Administrator,Moderator")]
    public class PitanjeController : MyController
    {
        [HttpGet]
        public ActionResult Izbrisi(int id, int lekcijaId)
        {
            Repository.AddAktivnost(new Aktivnost(User.Identity.GetUserId(), $"Briše pitanje (id: {id})", true));
            Repository.DeletePitanje(id);
            return RedirectToAction("pregled", "kviz", new { id = lekcijaId });
        }

        [HttpGet]
        public ActionResult Uredi(int id, int lekcijaId)
        {
            Repository.AddAktivnost(new Aktivnost(User.Identity.GetUserId(), $"Uređuje pitanje (id: {id})", true));
            throw new NotImplementedException();
            //Repository.DeletePitanje(id);
            return RedirectToAction("pregled", "kviz", new { id = lekcijaId });
        }
    }
}