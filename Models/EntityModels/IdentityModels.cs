﻿using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Security.Claims;
using System.Threading.Tasks;
using Meritokrat2.Models.AppModels;
using Meritokrat2.Models.AppModels.Kviz;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Meritokrat2.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Lekcija> Lekcijas { get; set; }
        public DbSet<UcenikLekcija> UcenikLekcijas { get; set; }
        public DbSet<Ucenik> Uceniks { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Kviz> Kvizs { get; set; }
        public DbSet<Odgovor> Odgovors { get; set; }
        public DbSet<Pitanje> Pitanjes { get; set; }
        public DbSet<Clanak> Clanaks { get; set; }

        public DbSet<Aktivnost> Aktivnosts { get; set; }


    }
}