﻿namespace Meritokrat2.Models.AppModels
{
    public class RezultatiSkole
    {
        public string Skola { get; set; }
        public int Bodovi { get; set; }
    }
}