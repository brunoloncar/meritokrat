﻿namespace Meritokrat2.Models.AppModels
{
    public class RezultatiPojedinacno
    {
        public string Nadimak { get; set; }
        public string Skola { get; set; }
        public int Razred { get; set; }
        public int Bodovi { get; set; }
    }
}