﻿using Meritokrat2.Data_Layer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meritokrat2.Models.AppModels
{
    public class Ucenik
    {
        public int UcenikId { get; set; }
        public String AaiId { get; set; }
        public String Nadimak { get; set; }
        public Likovi Lik { get; set; }
        public int Razred { get; set; }
        public char RazredSlovo { get; set; }
        public String Skola { get; set; }
        public DateTime ZadnjaOdigranaIgra { get; set; }

        public int Rezultat
        {
            get
            {
                return RepoFactory.GetRepo().GetRezultatUcenika(this.UcenikId);
            }
        }

        public bool MozeIgrati()
        {
            if (true)
            {
                TimeSpan ts = DateTime.Now - ZadnjaOdigranaIgra;
                if (ts.TotalDays < 1)
                {
                    return false;
                }
                return true;
            }
        }
    }
}