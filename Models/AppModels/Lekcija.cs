﻿using Meritokrat2.Data_Layer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Meritokrat2.Models.AppModels
{
    public class Lekcija
    {
        public Lekcija()
        {
            MaxBodovi = 0;
            RijesenaPuta = 0;
        }

        public int LekcijaId { get; set; }
        [Required(ErrorMessage ="Naziv je obvezan")]
        public String Naziv { get; set; }

        [Display(Name = "Redni broj")]
        public int RBLekcije { get; set; } //redni broj lekcije
        [Required(ErrorMessage ="Datoteka je obvezna")]
        [Display(Name = "Video ID")]
        public Guid IdVideo { get; set; }

        [Display(Name = "Kviz ID")]
        public int IdKviz { get; set; } = 0;

        public Guid IdClanak { get; set; }
        public int RijesenaPuta { get; set; }

        [Display(Name = "Maks. bodova")]
        public int MaxBodovi { get; set; }
        public int Razred { get; set; }

        public String GetVideoUrl()
        {
            return $"{IdVideo}.mp4";
        }

        public String Prosjek
        {
            get
            {
                return RepoFactory.GetRepo().GetProsjekLekcije(this.LekcijaId);
            }
        }
    }
}