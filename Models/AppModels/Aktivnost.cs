﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Meritokrat2.Models.AppModels
{
    public class Aktivnost
    {
        public Aktivnost(string userId, string opisAktivnosti, bool modAktivnost = false)
        {
            UserId = userId;
            OpisAktivnosti = opisAktivnosti;
            ModAktivnost = modAktivnost;
            this.DatumIVrijeme = DateTime.Now;
        }
        public Aktivnost()
        {
        }

        public int AktivnostId { get; set; }
        public String UserId { get; set; }
        public String OpisAktivnosti { get; set; }
        public bool ModAktivnost { get; set; }
        public DateTime DatumIVrijeme { get; set; }
    }
}