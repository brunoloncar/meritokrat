﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Meritokrat2.Models.AppModels
{
    public class Post
    {
        public int PostID { get; set; }

        [Required(ErrorMessage = "Autor je obvezan")]
        [Display(Name = "Autor")]
        public String Autor { get; set; }

        [Required(ErrorMessage = "Naslov je obvezan")]
        [Display(Name = "Naslov")]
        public String Naslov { get; set; }

        [Required(ErrorMessage ="Test je obvezan")]
        public String TextPost { get; set; }

        public Post()
        {

        }
    }
}