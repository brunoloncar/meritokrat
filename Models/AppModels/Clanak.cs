﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Meritokrat2.Models.AppModels
{
    public class Clanak
    {

        public int ClanakID { get; set; }

        [Required(ErrorMessage ="Autor je obvezan")]
        [Display(Name ="Autor")]
        public String Autor { get; set; }

        [Required(ErrorMessage ="Naslov je obvezan")]
        [Display(Name ="Naslov")]
        public String Naslov { get; set; }

        [Required(ErrorMessage ="Datoteka je obvezna")]
        [Display(Name ="File name")]
        public String ClanakFileName { get; set; }

        public Clanak()
        {

        }

    }
}