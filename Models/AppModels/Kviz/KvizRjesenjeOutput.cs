﻿namespace Meritokrat2.Models.AppModels.Kviz
{
    class KvizRjesenjeOutput
    {
        public int Tocnih { get; set; }
        public int Netocnih { get; set; }
        public int UkupnoBodova { get; set; }

        public KvizRjesenjeOutput(int tocnih, int netocnih, int ukupnoBodova)
        {
            this.Tocnih = tocnih;
            this.Netocnih = netocnih;
            this.UkupnoBodova = ukupnoBodova;
        }
    }
}