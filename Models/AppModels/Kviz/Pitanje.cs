﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Meritokrat2.Models.AppModels.Kviz
{
    public class Pitanje
    {
        public Pitanje()
        {
            Odgovori = new List<Odgovor>();
        }

        [Key]
        public int PitanjeId { get; set; }
        public int IdKviz { get; set; }
        public PitanjeType Tip { get; set; }
        public string TekstPitanja { get; set; }
        public virtual ICollection<Odgovor> Odgovori { get; set; }
        public int Vrijednost { get; set; } = 1;

        internal static bool JeliTocnoOdgovoreno(Pitanje pitanje, Odgovor odgovor)
        {
            //ako je pitanje sa jednim tocnim odgovorom
            if (pitanje.Tip == PitanjeType.Jedan_tocan)
            {
                //dohvacamo tocan odgovore
                var tocanOdgovor = pitanje.Odgovori.Where(x => x.Tocan).Single();

                //vracamo true ako je proslijeden odgovor jednak tocnom odgovoru.. inace false
                return tocanOdgovor.OdgovorId == odgovor.OdgovorId;
            }

            //ako je pitanje sa vise tocnih odgovora
            else if (pitanje.Tip == PitanjeType.Vise_tocnih)
            {
                //prvo razdijelimo odgovore (httppost ih salje odvojene zarezom)
                var _odgovori = odgovor.TekstOdgovora.Split(',');

                //dohvacamo tocne odgovore
                var tocniOdgovori = pitanje.Odgovori.Where(x => x.Tocan);

                //---------------------
                //PROVJERI RADI LI OVO
                //---------------------
                //prolazimo kroz sve tocne odgovore poslanog pitanja iz parametra
                //ako se medu poslanim odgovorima iz parametra ne nalazi tocan odgovor
                //vraca se false.. inace true
                foreach (var tocanOdgovor in tocniOdgovori)
                {
                    if (!_odgovori.ToList().Contains(tocanOdgovor.TekstOdgovora))
                    {
                        //vracamo false ako ne sadrzi tocan odgovor
                        return false;
                    }
                }
                return true;
            }

            //ako je pitanje nadopune
            else if (pitanje.Tip == PitanjeType.Nadopuni)
            {
                //dohvacamo moguce tocne odgovore
                var tocniOdgovori = pitanje.Odgovori.Where(x => x.Tocan);
                foreach (var tocanOdgovor in tocniOdgovori)
                {
                    if (tocanOdgovor.TekstOdgovora == odgovor.TekstOdgovora)
                    {
                        //vracamo true ako sadrzi tocan odgovor
                        return true;
                    }
                }
                //vracamo false ako ne sadrzi tocan odgovor
                return false;
            }

            //baci gresku ako je pitanje broken
            throw new Exception("Nepoznat tip pitanja.");
        }

        public override bool Equals(object obj)
        {
            var pitanje = obj as Pitanje;
            return pitanje.PitanjeId == PitanjeId && pitanje.Tip == Tip && TekstPitanja == TekstPitanja;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public enum PitanjeType
    {
        Jedan_tocan,
        Vise_tocnih,
        Nadopuni
    }
}