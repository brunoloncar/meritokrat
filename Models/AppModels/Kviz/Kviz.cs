﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace Meritokrat2.Models.AppModels.Kviz
{
    public class Kviz
    {
        [Key]
        public int KvizId { get; set; }
        public virtual ICollection<Pitanje> Pitanja { get; set; }
    }
}