﻿using System;

namespace Meritokrat2.Models.AppModels.Kviz
{
    public class Odgovor
    {
        public int OdgovorId { get; set; }
        public String TekstOdgovora { get; set; }
        public bool Tocan { get; set; }
        public int IdPitanje { get; set; }

        public override string ToString()
        {
            return $"{TekstOdgovora} ({Tocan})";
        }
    }
}