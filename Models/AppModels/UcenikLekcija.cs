﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Meritokrat2.Models.AppModels
{
    public class UcenikLekcija
    {
        public int UcenikLekcijaId { get; set; }
        public int IdUcenik { get; set; }

        public int IdLekcija { get; set; }
        public DateTime DatumRjesavanja { get; set; }
        public int Bodovi { get; set; }
        public String Biljeske { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            var _obj = obj as UcenikLekcija;
            return _obj.IdLekcija == IdLekcija && _obj.IdUcenik == IdUcenik;
            
        }

        public override int GetHashCode()
        {
            return IdUcenik % 10;
        }
    }
}