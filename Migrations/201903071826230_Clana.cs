namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Clana : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clanaks",
                c => new
                    {
                        ClanakID = c.Int(nullable: false, identity: true),
                        Autor = c.String(),
                        Naslov = c.String(),
                        ClanakFileName = c.String(),
                    })
                .PrimaryKey(t => t.ClanakID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Clanaks");
        }
    }
}
