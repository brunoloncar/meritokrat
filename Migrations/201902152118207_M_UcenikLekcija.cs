namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_UcenikLekcija : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UcenikLekcijas",
                c => new
                    {
                        UcenikLekcijaId = c.Int(nullable: false, identity: true),
                        IdUcenik = c.Int(nullable: false),
                        IdLekcija = c.Int(nullable: false),
                        DatumRjesavanja = c.DateTime(nullable: false),
                        Bodovi = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UcenikLekcijaId);
            
            AddColumn("dbo.Lekcijas", "RijesenaPuta", c => c.Int(nullable: false));
            AddColumn("dbo.Lekcijas", "MaxBodovi", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Lekcijas", "MaxBodovi");
            DropColumn("dbo.Lekcijas", "RijesenaPuta");
            DropTable("dbo.UcenikLekcijas");
        }
    }
}
