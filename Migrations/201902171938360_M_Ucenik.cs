namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Ucenik : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Uceniks",
                c => new
                    {
                        UcenikId = c.Int(nullable: false, identity: true),
                        AaiId = c.String(),
                        Nadimak = c.String(),
                        Lik = c.Int(nullable: false),
                        Razred = c.Int(nullable: false),
                        Skola = c.String(),
                        ZadnjaOdigranaIgra = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UcenikId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Uceniks");
        }
    }
}
