namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_UcenikLekcija1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UcenikLekcijas", "Biljeske", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.UcenikLekcijas", "Biljeske");
        }
    }
}
