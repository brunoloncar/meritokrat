// <auto-generated />
namespace Meritokrat2.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class validacijaJureCetvrtak : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(validacijaJureCetvrtak));
        
        string IMigrationMetadata.Id
        {
            get { return "201903141619550_validacijaJureCetvrtak"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
