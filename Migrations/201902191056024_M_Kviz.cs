namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Kviz : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Kvizs",
                c => new
                    {
                        KvizId = c.Int(nullable: false, identity: true),
                        PathToKviz = c.String(),
                    })
                .PrimaryKey(t => t.KvizId);
            
            CreateTable(
                "dbo.Pitanjes",
                c => new
                    {
                        PitanjeId = c.Int(nullable: false, identity: true),
                        IdKviz = c.Int(nullable: false),
                        Tip = c.Int(nullable: false),
                        TekstPitanja = c.String(),
                        Vrijednost = c.Int(nullable: false),
                        Kviz_KvizId = c.Int(),
                    })
                .PrimaryKey(t => t.PitanjeId)
                .ForeignKey("dbo.Kvizs", t => t.Kviz_KvizId)
                .Index(t => t.Kviz_KvizId);
            
            CreateTable(
                "dbo.Odgovors",
                c => new
                    {
                        OdgovorId = c.Int(nullable: false, identity: true),
                        TekstOdgovora = c.String(),
                        Tocan = c.Boolean(nullable: false),
                        Pitanje_PitanjeId = c.Int(),
                    })
                .PrimaryKey(t => t.OdgovorId)
                .ForeignKey("dbo.Pitanjes", t => t.Pitanje_PitanjeId)
                .Index(t => t.Pitanje_PitanjeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pitanjes", "Kviz_KvizId", "dbo.Kvizs");
            DropForeignKey("dbo.Odgovors", "Pitanje_PitanjeId", "dbo.Pitanjes");
            DropIndex("dbo.Odgovors", new[] { "Pitanje_PitanjeId" });
            DropIndex("dbo.Pitanjes", new[] { "Kviz_KvizId" });
            DropTable("dbo.Odgovors");
            DropTable("dbo.Pitanjes");
            DropTable("dbo.Kvizs");
        }
    }
}
