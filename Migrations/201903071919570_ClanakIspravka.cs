namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClanakIspravka : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clanaks", "Autor", c => c.String(nullable: false));
            AlterColumn("dbo.Clanaks", "Naslov", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Clanaks", "Naslov", c => c.String());
            AlterColumn("dbo.Clanaks", "Autor", c => c.String());
        }
    }
}
