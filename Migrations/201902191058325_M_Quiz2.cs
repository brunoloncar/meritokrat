namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Quiz2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Lekcijas", "IdKviz");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Lekcijas", "IdKviz", c => c.Guid(nullable: false));
        }
    }
}
