namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Aktivnost : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Aktivnosts",
                c => new
                    {
                        AktivnostId = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        OpisAktivnosti = c.String(),
                        ModAktivnost = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.AktivnostId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Aktivnosts");
        }
    }
}
