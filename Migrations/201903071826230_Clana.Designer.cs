// <auto-generated />
namespace Meritokrat2.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Clana : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Clana));
        
        string IMigrationMetadata.Id
        {
            get { return "201903071826230_Clana"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
