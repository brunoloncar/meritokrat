namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Razred : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Lekcijas", "Razred", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Lekcijas", "Razred");
        }
    }
}
