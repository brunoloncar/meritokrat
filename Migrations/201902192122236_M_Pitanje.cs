namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Pitanje : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Odgovors", "IdPitanje", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Odgovors", "IdPitanje");
        }
    }
}
