namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test_validacije : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Clanaks", "ClanakFileName", c => c.String(nullable: false));
            AlterColumn("dbo.Lekcijas", "Naziv", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Lekcijas", "Naziv", c => c.String());
            AlterColumn("dbo.Clanaks", "ClanakFileName", c => c.String());
        }
    }
}
