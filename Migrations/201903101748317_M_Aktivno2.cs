namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Aktivno2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Aktivnosts", "DatumIVrijeme", c => c.DateTime(nullable: false, defaultValueSql: "GETDATE()"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Aktivnosts", "DatumIVrijeme");
        }
    }
}
