namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class M_Quiz4 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Kvizs", "PathToKviz");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Kvizs", "PathToKviz", c => c.String());
        }
    }
}
