namespace Meritokrat2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PostsMigraton : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        PostID = c.Int(nullable: false, identity: true),
                        Autor = c.String(nullable: false),
                        Naslov = c.String(nullable: false),
                        TextPost = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.PostID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Posts");
        }
    }
}
